
      # Configuring SSL certificate for Caddy server

      This version of Caddy application uses pre-generated certificate which is self-signed and is meant to be
      only a temporary solution just to exemplify SSL/TLS configuration.

      Generate a new self-signed certificate for your Caddy server using `openssl` command
      or use a signed certifcate that you already own.

      For example, if you would like to generate self-signed certifcate and private key
      you could run the following command:
      ```
      openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout cert.key -out cert.crt
      ```

      Configure your own certificate using the following command:
      ```
      export CERT_FILE=cert.crt
      export KEY_FILE=cert.key
      export APP_INSTANCE_NAME=<name of your Caddy instance, e.g. caddy-1>
      export NAMESPACE=v2ray

      # using --dry-run option because we update existing secret resource
      # if we used kubectl create... for existing secret we would get an error
      kubectl --namespace v2ray create secret generic caddy-secret \
        --from-file=$CERT_FILE --from-file=$KEY_FILE \
        --dry-run -o yaml | kubectl apply -f -
      ```
      where .cert is a file containing SSL certificate and .key file contains private part of it.

      Run these commands to restart all Pods without causing a downtime
      ```
      PODS=$(kubectl get pods --namespace v2ray | awk 'FNR>1 {print $1}')

      TIMEOUT=60

      for i in ${PODS[@]}; do
        echo "Deleting Pod: $i..."
        kubectl delete pod $i --namespace v2ray
        echo "Sleeping for $TIMEOUT seconds..."
        sleep $TIMEOUT
      done
      ```
